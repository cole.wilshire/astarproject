// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASTARALGORITHM_AStarGameModeBase_generated_h
#error "AStarGameModeBase.generated.h already included, missing '#pragma once' in AStarGameModeBase.h"
#endif
#define ASTARALGORITHM_AStarGameModeBase_generated_h

#define AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_SPARSE_DATA
#define AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_RPC_WRAPPERS
#define AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAAStarGameModeBase(); \
	friend struct Z_Construct_UClass_AAStarGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAStarGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/AStarAlgorithm"), NO_API) \
	DECLARE_SERIALIZER(AAStarGameModeBase)


#define AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAAStarGameModeBase(); \
	friend struct Z_Construct_UClass_AAStarGameModeBase_Statics; \
public: \
	DECLARE_CLASS(AAStarGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/AStarAlgorithm"), NO_API) \
	DECLARE_SERIALIZER(AAStarGameModeBase)


#define AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAStarGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAStarGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAStarGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAStarGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAStarGameModeBase(AAStarGameModeBase&&); \
	NO_API AAStarGameModeBase(const AAStarGameModeBase&); \
public:


#define AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AAStarGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AAStarGameModeBase(AAStarGameModeBase&&); \
	NO_API AAStarGameModeBase(const AAStarGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AAStarGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AAStarGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AAStarGameModeBase)


#define AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_12_PROLOG
#define AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_SPARSE_DATA \
	AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_RPC_WRAPPERS \
	AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_INCLASS \
	AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_SPARSE_DATA \
	AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASTARALGORITHM_API UClass* StaticClass<class AAStarGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AStarAlgorithm_Source_AStarAlgorithm_AStarGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
