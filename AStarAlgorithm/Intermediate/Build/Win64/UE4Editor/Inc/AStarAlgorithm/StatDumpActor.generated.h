// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef ASTARALGORITHM_StatDumpActor_generated_h
#error "StatDumpActor.generated.h already included, missing '#pragma once' in StatDumpActor.h"
#endif
#define ASTARALGORITHM_StatDumpActor_generated_h

#define AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_SPARSE_DATA
#define AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_RPC_WRAPPERS
#define AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAStatDumpActor(); \
	friend struct Z_Construct_UClass_AStatDumpActor_Statics; \
public: \
	DECLARE_CLASS(AStatDumpActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AStarAlgorithm"), NO_API) \
	DECLARE_SERIALIZER(AStatDumpActor)


#define AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAStatDumpActor(); \
	friend struct Z_Construct_UClass_AStatDumpActor_Statics; \
public: \
	DECLARE_CLASS(AStatDumpActor, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/AStarAlgorithm"), NO_API) \
	DECLARE_SERIALIZER(AStatDumpActor)


#define AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AStatDumpActor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AStatDumpActor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStatDumpActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStatDumpActor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStatDumpActor(AStatDumpActor&&); \
	NO_API AStatDumpActor(const AStatDumpActor&); \
public:


#define AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AStatDumpActor(AStatDumpActor&&); \
	NO_API AStatDumpActor(const AStatDumpActor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AStatDumpActor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AStatDumpActor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AStatDumpActor)


#define AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_PRIVATE_PROPERTY_OFFSET
#define AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_9_PROLOG
#define AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_PRIVATE_PROPERTY_OFFSET \
	AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_SPARSE_DATA \
	AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_RPC_WRAPPERS \
	AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_INCLASS \
	AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_PRIVATE_PROPERTY_OFFSET \
	AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_SPARSE_DATA \
	AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_INCLASS_NO_PURE_DECLS \
	AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> ASTARALGORITHM_API UClass* StaticClass<class AStatDumpActor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID AStarAlgorithm_Source_AStarAlgorithm_StatDumpActor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
