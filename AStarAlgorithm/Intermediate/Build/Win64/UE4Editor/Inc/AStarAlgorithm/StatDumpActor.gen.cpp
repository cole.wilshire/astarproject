// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "AStarAlgorithm/StatDumpActor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeStatDumpActor() {}
// Cross Module References
	ASTARALGORITHM_API UClass* Z_Construct_UClass_AStatDumpActor_NoRegister();
	ASTARALGORITHM_API UClass* Z_Construct_UClass_AStatDumpActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_AStarAlgorithm();
// End Cross Module References
	void AStatDumpActor::StaticRegisterNativesAStatDumpActor()
	{
	}
	UClass* Z_Construct_UClass_AStatDumpActor_NoRegister()
	{
		return AStatDumpActor::StaticClass();
	}
	struct Z_Construct_UClass_AStatDumpActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AStatDumpActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_AStarAlgorithm,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AStatDumpActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "StatDumpActor.h" },
		{ "ModuleRelativePath", "StatDumpActor.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AStatDumpActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AStatDumpActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AStatDumpActor_Statics::ClassParams = {
		&AStatDumpActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AStatDumpActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AStatDumpActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AStatDumpActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AStatDumpActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AStatDumpActor, 3264228121);
	template<> ASTARALGORITHM_API UClass* StaticClass<AStatDumpActor>()
	{
		return AStatDumpActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AStatDumpActor(Z_Construct_UClass_AStatDumpActor, &AStatDumpActor::StaticClass, TEXT("/Script/AStarAlgorithm"), TEXT("AStatDumpActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AStatDumpActor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
