// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "AStarGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ASTARALGORITHM_API AAStarGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	double SecondsPerCycle = 0.0;
	double SecondsPerCycle64 = 0.0;

	double GetSecondsPerCycle64();

	TCHAR* StrDate(TCHAR* Dest, SIZE_T DestSize);

	TCHAR* StrTime(TCHAR* Dest, SIZE_T DestSize);

	const TCHAR* StrTimestamp();

	FString PrettyTime(double Seconds);
};
